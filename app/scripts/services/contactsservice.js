'use strict';

/**
 * @ngdoc service
 * @name contactsApp.contactsService
 * @description
 * # contactsService
 * Service in the contactsApp.
 */
angular.module('contactsApp')
  .service('contactsService', function ($http, $q) {

    // var baseURL = 'http://localhost:3000'
    var baseURL = 'http://35.165.164.234:3000'
    var exampleContacts = [
      {'name': 'Suzanne Buhr', 'email': 'suzanne@gmail.com', 'phone': '(155) 879-1033'},
      {'name': 'Monet Knack', 'email': 'monet@gmail.com', 'phone': '(802) 522-8955'},
      {'name': 'Bertha Headlee', 'email': 'bertha@gmail.com', 'phone': '(895) 458-0639'},
      {'name': 'Karen Wild', 'email': 'karen@gmail.com', 'phone': '(826) 627-1026'},
      {'name': 'Rosette Beckett', 'email': 'rosette@gmail.com', 'phone': '(197) 807-4582'}
    ];
    return {
      save: function(contact) {
        var p = $http.post(baseURL + '/new', contact).then(function(res){
            return res.data;
          },
          function(err){
            console.log(err);
        });
        return p;
      },
      get: function() {
        var p = $http.get(baseURL + '/').then(function(res){
            return res.data;
          },
          function(err){
            console.log(err);
        });
        return p;
      },
    };
  });
