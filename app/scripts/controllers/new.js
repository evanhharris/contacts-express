'use strict';

/**
 * @ngdoc function
 * @name contactsApp.controller:NewCtrl
 * @description
 * # NewCtrl
 * Controller of the contactsApp
 */
angular.module('contactsApp')
  .controller('NewCtrl', function ($scope, $location, contactsService) {
    var contact = {
      'email': '',
      'phone': '',
      'name': '',
    };
    $scope.contact = contact;
    $scope.validate = function() {
      var completed = true;
      if ($scope.contact.email == '' || $scope.contact.email == '' || $scope.contact.name == ''){
        completed = false;
      }
      if (completed && $scope.newContact.$valid){
        contactsService.save($scope.contact);
        $location.path('/')
      }
      else {
        $scope.contact = contact;
      }
    };
  });
