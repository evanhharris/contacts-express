# contacts

- [yo angular generator](https://github.com/yeoman/generator-angular)
version 0.15.1 for scaffolding.

- AngularJS and Bootstrap for the front-end

- Express and Cassandra for the backend. For testing the DB, I used the public [Cassandra Dockerfile](https://hub.docker.com/_/cassandra/) and shared the access port.

```
docker run --name cassandradb -p 9042:9042 cassandra
```


## Features

- Add/save contacts to Cassandra DB
- Filter by name, number, or email
- Form validation with Angular

## Running

This uses the Angular/Grunt/Node stack so to install...
```
npm install
```
```
bower install
```

And run the webapp
```
grunt serve
```

The express app
```
node app.js
```

And Cassandra in a container
```
docker pull cassandra; docker run -p 9042:9042 -d --name cassandradb cassandra
```

A live version running at http://contacts-express.s3-website-us-west-2.amazonaws.com/#!/. This is using
a lightweight docker host on EC2 for the express application and a Cassandra instance for storage.
