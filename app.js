var express = require('express')
var app = express()
var models = require('express-cassandra');
var bodyParser = require('body-parser')

app.use(bodyParser.json())
app.use(bodyParser.urlencoded({ extended: false }))


app.use(function(req, res, next) {
  res.header("Access-Control-Allow-Origin", "*");
  res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
  next();
});

// Cassandra setup
models.setDirectory( __dirname + '/models').bind(
    {
        clientOptions: {
            contactPoints: ['127.0.0.1'],
            protocolOptions: { port: 9042 },
            keyspace: 'contactskeyspace',
            queryOptions: {consistency: models.consistencies.one}
        },
        ormOptions: {
            //If your keyspace doesn't exist it will be created automatically
            //using the default replication strategy provided here.
            defaultReplicationStrategy : {
                class: 'SimpleStrategy',
                replication_factor: 1
            },
            migration: 'safe',
            createKeyspace: true
        }
    },
    function(err) {
        if(err) console.log(err.message);
        else console.log(models.timeuuid());
    }
);


app.get('/', function(req, res) {
  models.instance.Contact.find({cached: true}, {raw: true, allow_filtering: true}, function(err, contacts){
      res.send(contacts)
  });
});

app.post('/new', function(req, res) {

  var newContact = new models.instance.Contact({
      name: req.body.name,
      email: req.body.email,
      phone: req.body.phone,
      cached: true,
  });
  newContact.save(function(err){
      if(err) {
          console.log(err);
          return;
      }
      console.log('success');
  });
  res.send('OK')
});

app.listen(3000, function () {
  console.log('Listening on port 3000!')
})
